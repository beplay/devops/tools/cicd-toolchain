#!/bin/bash

shopt -s expand_aliases

if [[ -f /etc/profile ]]; then
  source /etc/profile
fi