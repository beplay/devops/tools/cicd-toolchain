FROM	alpine:latest

ENV USER oper
ENV BASH_ENV "/home/${USER}/.bashrc"

ADD ./resources/cacerts/* /usr/local/share/ca-certificates/
ADD ./resources/dotfiles /tmp/dotfiles
ADD ./resources/etc/ /etc/

RUN \
    # Install software
    apk add --no-cache --virtual=.build-dependencies \
      curl \
      git \
      go \ 
    && \
    # Install k8split
    go get -u github.com/brendanjryan/k8split && \
    mv ~/go/bin/* /usr/local/bin/ && \
    rm -rf ~/go && \
    \
    # Install kubectl ( latest )
    curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl && \
    chmod +x ./kubectl && \
    mv ./kubectl /usr/local/bin/kubectl && \
    \
    # Install kustomize
    KUSTOMIZE_VERSION=4.0.5 && \
    curl -L https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv${KUSTOMIZE_VERSION}/kustomize_v${KUSTOMIZE_VERSION}_linux_amd64.tar.gz -o /tmp/kustomize.tar.gz && \
    tar -C /usr/local/bin/ -xvf /tmp/kustomize.tar.gz && \
    chmod +x /usr/local/bin/kustomize && \
    \
    # Install yq latest version
    curl -L https://github.com/mikefarah/yq/releases/download/$(curl -s https://github.com/mikefarah/yq/releases/latest | grep -o -E "tag/[^\"]+" | sed 's/tag\///' )/yq_linux_amd64 -o /usr/local/bin/yq && \
    chmod +x /usr/local/bin/yq && \
    \
    # Remove unwanted software 
    apk del .build-dependencies && \
    \
    # Install additional tools
    apk add \
      bash \
      ca-certificates \
      curl \
      jq \
      openssh-client \
      sshpass \
      && \
    \
    # Install ca-certificate
    update-ca-certificates && \
    \
    # Add user 
    addgroup -S ${USER} && \
    adduser -S ${USER} -G ${USER} -h /home/${USER} && \
    \ 
    # Install dotfiles
    cp -a /tmp/dotfiles/.[^.]* /root; \
    cp -a /tmp/dotfiles/.[^.]* /home/${USER}; \
    \ 
    # Remove tmp and cache
    rm -rf /var/cache/apk/* /tmp/* /var/tmp/* 

WORKDIR /home/${USER}
USER ${USER}
CMD [ "/bin/bash" ]